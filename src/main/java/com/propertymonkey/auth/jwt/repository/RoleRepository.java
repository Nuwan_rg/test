package com.propertymonkey.auth.jwt.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.propertymonkey.auth.jwt.model.ERole;
import com.propertymonkey.auth.jwt.model.Role;


public interface RoleRepository extends MongoRepository<Role, String> {
  Optional<Role> findByName(ERole name);
}
package com.propertymonkey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication
public class PropertyMonkeyApplication {

	public static void main(String[] args) {
		SpringApplication.run(PropertyMonkeyApplication.class, args);
	}

}
